import React, { Component } from 'react';

export class Input extends Component{
  constructor(props){
    super(props);
    this.state = {
      type: this.props.type || null,
      name: this.props.name || null,
      value: this.props.value || null,
      placeholder: this.props.placeholder || null
    }

    this.requiredInput = this.requiredInput.bind(this);

    this.renderInputType = this.renderInputType.bind(this);
    this.renderInput = this.renderInput.bind(this);
  }

  componentWillMount(){

  }

  componentDidMount(){

  }

  requiredInput(e){
    if(!e.target.value || e.target.value === ''){
      e.target.classList.add('required');
    }else{
      e.target.classList.remove('required');
    }
    return false;
  };//requiredInput(e);

  renderInputType(){
    return (
      <input
        type={this.state.type}
        name={this.state.name}
        ref={this.state.name}
        value={this.state.value}
        placeholder={this.state.placeholder}
        />
    );
  };//renderInputType();

  renderInput(){
    if(this.state.type === 'Input'){
      return this.renderInputType;
    }
  };//renderInput();

  render(){
    return (
      this.renderInput
    );
  }
}
