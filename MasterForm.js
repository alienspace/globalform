import React, { Component } from 'react';

export class MasterForm extends Component{
  constructor(props){
    super(props);
    this.state = {
      onLoadForm: this.props.onLoadForm || null,
      onChangeForm: this.props.onChangeForm || null,
      onSubmitForm: this.props.onSubmitForm || null,
      action: this.props.action || null,
      type: this.props.type || 'Form',
      name: this.props.name || null,
      value: this.props.value || null,
      placeholder: this.props.placeholder || null
    }

    // this.requiredInput = this.requiredInput.bind(this);
    //
    this.getPages = this.getPages.bind(this);
    this.renderFormType = this.renderFormType.bind(this);
    // this.renderForm = this.renderForm.bind(this);
  }

  componentWillMount(){
    if(this.state.onLoadForm)
      return this.state.onLoadForm();
  }

  componentDidMount(){
  }

  getPages(pageID){
    // eslint-disable-next-line
    pageID = parseInt(pageID);
    if( pageID ){
      console.log('getPages pageID ->',pageID);
      // eslint-disable-next-line
      return this.state.pages.map((page) => {
        if(page.pageID === pageID){
          this.setState({
            documentCurrent: {
              pageID : page.pageID,
              title : page.title,
              slug : page.slug,
              content : page.content,
              typeContent : page.typeContent,
              category : page.category,
              status: page.status
            }
          });
          return this.state.documentCurrent;
        }else{
          return false;
        }
      });
    }
    this.setState({ isLoading : false });
  };//getPages();

  // requiredInput(e){
  //   if(!e.target.value || e.target.value === ''){
  //     e.target.classList.add('required');
  //   }else{
  //     e.target.classList.remove('required');
  //   }
  //   return false;
  // };//requiredInput(e);
  //
  renderFormType(){
    if(this.state.type === 'Form'){
      return (
        <form name={this.state.name} action={this.state.action} onChange={this.state.onChangeForm} onSubmit={this.state.onSubmitForm}>
          {this.props.children}
        </form>
      );
    }else{
      return (<h1>FormType</h1>);
    }
  };//renderFormType();

  // renderForm(){
  //   if(this.state.type === 'Form'){
  //     return this.renderFormType;
  //   }
  // };//renderForm();

  render(){
    return this.renderFormType();
  }
}

MasterForm.defaultProps = {
  onLoadForm: null,
  onChangeForm: null,
  onSubmitForm: null,
  action: null,
  type: 'Form',
  name: null,
  value: null,
  placeholder: null
}
